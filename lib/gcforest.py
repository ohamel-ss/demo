import math
import numpy as np


import  tensorflow        as tf
from    tqdm              import tqdm, trange
import  keras
import  sklearn
import  sklearn.cluster
import  sklearn.preprocessing as pp

from gcforest.gcforest import GCForest


Sequential = keras.models.Sequential
KModel     = keras.models
KLayers    = keras.layers
KOpt       = keras.optimizers







tf_config = tf.ConfigProto()
tf_config.gpu_options.allow_growth = True
tf.Session(config = tf_config).as_default()


def rank_loss(y_true, y_pred):
  regress_exp     = 3
  #base_rank_ratio = 1#0.01#25#33
  # base_loss       = keras.losses.mean_absolute_error(y_true, y_pred)
  # base_loss       = np.prod([ base_loss ] * regress_exp, axis = 0)
  base_loss = 0
  #if base_rank_ratio == 1: return base_loss

  N_OUTPUTS       = 1
  y_true_flat     = tf.reshape(y_true, [ -1, N_OUTPUTS ])
  y_pred_flat     = tf.reshape(y_pred, [ -1, N_OUTPUTS ])

  # flip_shape  = [N_SEQ_PROBES, N_SEQ_PROBES]
  # rank_flip   = np.tril(-np.ones(flip_shape)).reshape([1] * (len(pred_rank.shape) - 2) + flip_shape)
  # tf.constant(rank_flip)

  def rank_loss_feature(true, pred):
    def ranking(xs):
      a = tf.expand_dims(xs, axis = -1)
      b = tf.expand_dims(xs, axis = -2)
      return a - b

    true_rank   = ranking(true)
    pred_rank   = ranking(pred)
    rank_mask   = tf.sign(true_rank) != tf.sign(pred_rank)
    rank_masked = tf.abs(true_rank - pred_rank) * tf.to_float(rank_mask)

    # EPSILON           = 1e-8
    # pred_rank_masked  = pred_rank_masked / (tf.norm(pred_rank_masked, axis = -1, ord = 1, keep_dims = True) + EPSILON)
    # pred_rank_normed  = pred_rank / (tf.norm(pred_rank, axis = -1, ord = 1, keep_dims = True) + EPSILON)

    err_sq            = tf.reduce_mean(rank_masked, axis = [-2, -1])
    return err_sq

  def softmax_feature(true, pred):
    softmax           = tf.nn.log_softmax(pred) - tf.nn.log_softmax(true)
    return tf.reduce_mean(tf.abs(softmax))

  rank_loss_ = tf.stack([ #rank_loss_feature(x, y) + softmax_feature(x, y)
                         rank_loss_feature(x, y)
                         for x, y in zip(tf.unstack(y_true_flat, axis = -1)
                                        ,tf.unstack(y_pred_flat, axis = -1)) ]
                      ,axis = -1)
  rank_loss_ = tf.reduce_sum(tf.reshape(rank_loss_, [ -1, ]), axis = -1)

  #return base_loss * base_rank_ratio + rank_loss_ * (1 - base_rank_ratio)
  return rank_loss_ # * (1 - base_rank_ratio)


def pairwise_mean_sqr_err(y_true, y_pred):
  return tf.losses.mean_pairwise_squared_error(tf.reshape(y_true, [1, -1]), tf.reshape(y_pred, [1, -1]))



def stats(y, *args):
  print(*args, end = '')
  parts = [ y.min(), y.max(), y.mean(), y.std() ]
  print('   '.join( '{:12g}'.format(x) for x in parts ))

def stats_all(xs):
  if len(xs.shape) == 1:
    xs = np.expand_dims(xs, -1)

  for i in range(xs.shape[-1]):
    stats(xs.reshape( [ -1, xs.shape[-1] ] )[:, i])






_load_data_cache = None
def load_data():
  global _load_data_cache
  if _load_data_cache is not None: return _load_data_cache

  fn    = 'LAL-randomtree-simulatedunbalanced-big.npz'
  # we found these parameters by cross-validating the regressor and now we reuse these expreiments
  data  = np.load('./lal datasets/'+fn)
  X     = data['arr_0']
  y     = data['arr_1']

  #X = X[:, :-1]

  def unitise(X): return X / np.amax(np.abs(X), axis = 0)
  def clean  (x): return pp.scale(x, with_std = False)

  X, y  = tuple(map(clean, (X, y) ))
  #y = clean(np.exp(y))
  print('X'); stats_all(X)
  print('y'); stats_all(y)

  # idxs      = np.arange(X.shape[0])
  # np.random.shuffle(idxs)
  # n_test    = int(len(idxs) * 0.33)
  # X, X_test = X[idxs[n_test:]], X[idxs[:n_test]]
  # y, y_test = y[idxs[n_test:]], y[idxs[:n_test]]
  _load_data_cache = X, y
  return _load_data_cache






X, y = load_data()

#y = y * 10
#y = y * y * y

idxs      = np.arange(X.shape[0])
np.random.shuffle(idxs)
n_test    = int(len(idxs) * 0.33 * 1)
X_train, X_test = X[idxs[n_test:]], X[idxs[:n_test]]
y_train, y_test = y[idxs[n_test:]], y[idxs[:n_test]]





def get_toy_config():

  ca_config = {}
  ca_config["random_state"] = 0
  ca_config["max_layers"] = 40
  ca_config["early_stopping_rounds"] = 3
  ca_config["n_classes"] = 1
  ca_config["estimators"] = [
    # {"n_folds": 5, "type": "XGBClassifier", "n_estimators": 10, "max_depth": 5, "objective": "multi:softprob", "silent": True, "nthread": -1, "learning_rate": 0.1},
    {"n_folds": 5, "type": "RandomForestClassifier", "n_estimators": 10, "max_depth": None, "n_jobs": -1},
    {"n_folds": 5, "type": "ExtraTreesClassifier", "n_estimators": 10, "max_depth": None, "n_jobs": -1},
    {"n_folds": 5, "type": "LogisticRegression"},
  ]

  return {
    'cascade': ca_config
  }



def mk_model():
  X   = KLayers.Input(batch_shape = [ None, load_data()[0].shape[-1] ])
  fn  = lambda x: x #KLayers.Activation('relu')#keras.activations.selu)

  Ws  = [ 2 ** 10 ] * 3#20
  print(Ws)

  y   = X
  # y   = KLayers.Dropout(0.3)(
  #   RBFLayer(2 ** 11, beta = 1)(
  #     y))

  for i, W in enumerate(Ws):
    perc      = 1#1 - (i / (len(Ws) - 1))
    droprate  = 0#0.2 * perc

    y = fn(
      KLayers.Dropout(droprate)(
        Treelike(W)(#, bias_initializer = 'glorot_uniform')(#, kernel_regularizer = 'l1', bias_regularizer = 'l1')(
          y)))

  y = KLayers.Dense(1, use_bias = True)(y)

  mdl = KModel.Model(inputs  = [ X ]
                    ,outputs = [ y ])

  def mean_3rd_err(y_true, y_pred):
    r = keras.losses.mean_absolute_error(y_true, y_pred)
    return r * r * r

  #mean_3rd_err#'mean_squared_error'
  loss_fn = 'mean_squared_error'#rank_loss  # 'mean_absolute_error'
  mdl.compile(loss = loss_fn, optimizer = KOpt.Adam(lr = 1e-5))
  return mdl





mdl = mk_model()
mdl.fit(X_train, y_train
      ,epochs = 15
      #,validation_split = 0.33
      ,batch_size = 10000)


#X_test, y_test = X, y


print()
print('TRAIN: ')
print()

pred = mdl.predict(X)#, batch_size = regression_features.shape[0] // 10)
pred = np.squeeze(pred, axis = -1)
diff = pred - y
stats(y, 'y: ')
stats(pred, 'pred: ')
stats(diff, 'error: ')


# import pickle
# with open("weights_sancheck_fc", 'wb') as f:
#   pickle.dump(mdl.get_weights(), f)


print()
if n_test == 0: exit(0)

print()
print('TEST: ')
print()

pred = mdl.predict(X_test)#, batch_size = regression_features.shape[0] // 10)
pred = np.squeeze(pred, axis = -1)
diff = pred - y_test
stats(y, 'y_test: ')
stats(pred, 'pred: ')
stats(diff, 'error: ')


def do_ranking_err(true, pred):
  def ranking(xs):
    a = np.expand_dims(xs, axis = -1)
    b = np.expand_dims(xs, axis = -2)
    return a - b

  true_rank = ranking(true)
  pred_rank = ranking(pred)
  err_rank  = np.sign(true_rank) != np.sign(pred_rank)
  err_rel_true  = np.abs(err_rank * true_rank)
  err_rel_pred  = np.abs(err_rank * pred_rank)

  n_max_errs      = len(pred) * (len(pred) - 1) / 2
  n_errors        = np.sum(err_rank     ) / 2
  n_err_rel_true  = np.sum(err_rel_true ) / 2
  n_err_rel_pred  = np.sum(err_rel_pred ) / 2

  return n_err_rel_true, n_err_rel_pred, n_errors, n_max_errs


def direct_sample(true, pred):
  return np.argmax(true) == np.argmax(pred)

def max_of_err(true, pred):
  def log_softmax(x):
    THETA = 2.0
    return x - np.log(np.sum(np.exp(x * THETA)))

  return np.abs(log_softmax(pred) - log_softmax(true))


idxs    = np.arange(len(y_test))
np.random.shuffle(idxs)

pred    = pred  [idxs]
y_test  = y_test[idxs]

n_direct      = 0
n_softmax     = 0
n_err_rel_true= 0
n_err_rel_pred= 0
n_errors      = 0
n_max_errs    = 0
n_batch_size  = 128 # 4096

with trange(math.ceil(len(y_test) / n_batch_size)) as pbar:
  for i in pbar:
    a = y_test[i * n_batch_size : (i+1) * n_batch_size]
    b = pred  [i * n_batch_size : (i+1) * n_batch_size]

    # n_direct   += direct_sample(a, b)
    # msg = 'direct correct: %g' % (n_direct / (1+i))

    # n_softmax += np.sum(max_of_err(a, b))
    # msg = 'softmax mean: %g' % (n_softmax / (1+i))

    e_rel_true, e_rel_pred, e, e_max = do_ranking_err(a, b)
    n_err_rel_true += e_rel_true
    n_err_rel_pred += e_rel_pred
    n_errors   += e
    n_max_errs += e_max
    msg = '# of ranking errors: %d of %d  (%g)  (%g / %g)' % (n_errors, n_max_errs, n_errors / n_max_errs, n_err_rel_true, n_err_rel_pred)

    pbar.set_description(msg)

print(msg)